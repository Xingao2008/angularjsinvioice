<?php
namespace Model;


class Invoice implements \JsonSerializable
{
    private $id;
    private $invoice_name;
    private $invoice_date;

    public function __construct($name)
    {
        $this->invoice_name = $name;
    }

    /**
     * @return mixed
     */
    public function getInvoiceName()
    {
        return $this->invoice_name;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    public function setInvoiceDate($date)
    {
        $this->invoice_date = $date;
    }

    /**
     * @return mixed
     */
    public function getInvoiceDate()
    {
        return $this->invoice_date;
    }

    public function jsonSerialize()
    {
        return array(
            'id' => $this->id,
            'invoice_name' => $this->invoice_name,
            'invoice_date' => $this->invoice_date,
        );
    }
}