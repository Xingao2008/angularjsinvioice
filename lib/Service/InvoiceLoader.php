<?php
namespace Service;


use Model\Invoice;
use XBase\Table;

class InvoiceLoader implements InvoiceLoaderInterface
{
    private $invoiceStorage;

    public function __construct(PdoInvoiceStorage $invoiceStorage)
    {
        $this->invoiceStorage = $invoiceStorage;
    }

    public function storeInvoice($path)
    {
        $insertArray = array();
        // use extenal dbf library
        $dbf = new Table($path);
        $i = 0;
        while ($record = $dbf->nextRecord()) {
            $subRecord = explode("      ", $record->datahold);
            if (substr($subRecord[0], 0, 8) === 'THLIQUOR') {
                $insertArray['invoice']['invoice_name'] = $subRecord[0];
                $insertArray['invoice']['invoice_date'] = $subRecord[1];
            }

            if (substr($subRecord[0], 0, 2) === 'FD') {
                if(substr($subRecord[1], 0, 5) === '00000') {
                    $insertArray['invoice_subitem'][$subRecord[0]]['number'] = $subRecord[0];
                    $insertArray['invoice_subitem'][$subRecord[0]]['order_number'] = $subRecord[1];
                    $insertArray['invoice_subitem'][$subRecord[0]]['address'] = $subRecord[3];
                    $insertArray['invoice_subitem'][$subRecord[0]]['suburb'] = $subRecord[4];
                    $insertArray['invoice_subitem'][$subRecord[0]]['invoice_number'] = $subRecord[6];
                    $insertArray['invoice_subitem'][$subRecord[0]]['delivery_date'] = $subRecord[7];
                } else {
                    $insertArray['product'][$subRecord[0]][$i]['number']=$subRecord[0];
                    $insertArray['product'][$subRecord[0]][$i]['description']=$subRecord[1];
                }
            }
            $i++;
        }

        $saveInvoice = $this->invoiceStorage;
        return $saveInvoice->store($insertArray);
    }

    /**
     * @return Invoice[]
     */
    public function get()
    {
        $invoices = array();

        $invoicesData = $this->invoiceStorage->fetchAllInvoiceData();

        foreach($invoicesData as $invoiceData) {
            $invoices[] = $this->createInvoiceFromData($invoiceData);
        }

        return $invoices;
    }

    /**
     * @param $id
     * @return Invoice
     */
    public function findOneById($id)
    {
        $invoiceArray = $this->invoiceStorage->fetchSingleInvoiceData($id);

        $singleInvoice = $this->createInvoiceFromData($invoiceArray);

        return $singleInvoice;
    }

    public function createInvoiceFromData(array $invoiceData)
    {
        $invoice = new Invoice($invoiceData['invoice_name']);
        $invoice->setId($invoiceData['id']);
        $invoice->setInvoiceDate($invoiceData['invoice_date']);

//        return $invoice;
        return $invoice->jsonSerialize();
    }
}