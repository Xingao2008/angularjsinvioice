<?php
namespace Service;

use PDO;

class PdoInvoiceSubItemStorage extends PdoInvoiceAbstract
{
    public function fetchAllInvoiceData()
    {
        $statement = $this->getPDO()->prepare('SELECT * FROM invoice_subItem');
        $statement->execute();

        return $statement->fetchAll(PDO::FETCH_ASSOC);
    }
}