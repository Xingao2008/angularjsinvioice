<?php
namespace Service;

use PDO;

class PdoProductStorage extends PdoInvoiceAbstract
{
    public function fetchAllInvoiceData($number)
    {
        $statement = $this->getPDO()->prepare('SELECT * FROM product WHERE number = :number');
        $statement->execute(array('number' => $number));

        return $statement->fetchAll(PDO::FETCH_ASSOC);
    }
}