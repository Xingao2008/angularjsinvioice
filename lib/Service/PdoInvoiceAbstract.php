<?php
namespace Service;

use PDO;

class PdoInvoiceAbstract
{
    private $pdo;

    public function __construct(PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    public function getPDO()
    {
        return $this->pdo;
    }
}