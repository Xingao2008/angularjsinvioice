<?php
namespace Service;


interface InvoiceLoaderInterface
{
    public function get();
    public function createInvoiceFromData(Array $array);
}