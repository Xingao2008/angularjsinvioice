<?php
namespace Service;

use PDO;

class Container
{
    private $configuration;
    private $pdo;
    private $invoiceLoader;
    private $invoiceStorage;
    private $invoiceSubItemLoader;
    private $invoiceSubItemStorage;
    private $productLoader;
    private $productStorage;

    public function __construct(array $configuration)
    {
        $this->configuration = $configuration;
    }

    /**
     * @return PDO
     */
    public function getPDO()
    {
        if ($this->pdo === null) {
            $this->pdo = new PDO(
                $this->configuration['db_dsn'],
                $this->configuration['db_user'],
                $this->configuration['db_pass']
            );

            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }

        return $this->pdo;
    }

    public function getInvoiceLoader()
    {
        if ($this->invoiceLoader === null) {
            $this->invoiceLoader = new InvoiceLoader($this->getInvoiceStorage());
        }

        return $this->invoiceLoader;
    }

    private function getInvoiceStorage()
    {
        if($this->invoiceStorage === null) {
            $this->invoiceStorage = new PdoInvoiceStorage($this->getPDO());
        }

        return $this->invoiceStorage;
    }

    public function getInvoiceSubItemLoader()
    {
        if ($this->invoiceSubItemLoader === null) {
            $this->invoiceSubItemLoader = new InvoiceSubItemLoader($this->getInvoiceSubItemStorage());
        }

        return $this->invoiceSubItemLoader;
    }

    private function getInvoiceSubItemStorage()
    {
        if ($this->invoiceSubItemStorage === null) {
            $this->invoiceSubItemStorage = new PdoInvoiceSubItemStorage($this->getPDO());
        }

        return $this->invoiceSubItemStorage;
    }

    /**
     * @param $number
     * @return InvoiceProductLoader
     */
    public function getProductLoader($number)
    {
        if ($this->productLoader === null) {
            $this->productLoader = new InvoiceProductLoader($this->getProductStorage(), $number);
        }

        return $this->productLoader;
    }

    private function getProductStorage()
    {
        if ($this->productStorage === null) {
            $this->productStorage = new PdoProductStorage($this->getPDO());
        }

        return $this->productStorage;
    }
}