<?php
namespace Service;


use Model\InvoiceSubItem;

class InvoiceSubItemLoader implements InvoiceLoaderInterface
{
    private $invoiceSubItemStorage;

    public function __construct(PdoInvoiceSubItemStorage $invoiceSubItemStorage)
    {
        $this->invoiceSubItemStorage = $invoiceSubItemStorage;
    }

    /**
     * @return InvoiceSubItem[]
     */
    public function get()
    {
        $invoiceSubItems = array();

        $invoicesData = $this->invoiceSubItemStorage->fetchAllInvoiceData();

        foreach($invoicesData as $invoiceData) {
            $invoiceSubItems[] = $this->createInvoiceFromData($invoiceData);
        }

        return $invoiceSubItems;
    }

    public function createInvoiceFromData(Array $invoiceData)
    {
        $invoiceSubItem = new InvoiceSubItem($invoiceData['number']);
        $invoiceSubItem->setId($invoiceData['id']);
        $invoiceSubItem->setOrderNumber($invoiceData['order_number']);
        $invoiceSubItem->setAddress($invoiceData['address']);
        $invoiceSubItem->setSuburb($invoiceData['suburb']);
        $invoiceSubItem->setInvoiceNumber($invoiceData['invoice_number']);
        $invoiceSubItem->setDeliveryDate($invoiceData['delivery_date']);

//        return $invoiceSubItem;
        return $invoiceSubItem->jsonSerialize();
    }

}