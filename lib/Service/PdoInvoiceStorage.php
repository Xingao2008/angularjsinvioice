<?php
namespace Service;

use PDO;

class PdoInvoiceStorage extends PdoInvoiceAbstract
{
    public function fetchAllInvoiceData()
    {
        $statement = $this->getPDO()->prepare('SELECT * FROM invoice');
        $statement->execute();

        return $statement->fetchAll(PDO::FETCH_ASSOC);
    }

    public function fetchSingleInvoiceData($id)
    {
        $statement = $this->getPDO()->prepare('SELECT * FROM invoice WHERE id = :id');
        $statement->execute(array('id' => $id));
        $invoiceArray = $statement->fetch(PDO::FETCH_ASSOC);

        if (!$invoiceArray) {
            return null;
        }

        return $invoiceArray;
    }

    public function store(Array $array)
    {
//         insert invoice overall info
        $invoice = $array['invoice'];
        $invoice_statement = $this->getPDO()->prepare('INSERT INTO invoice (invoice_name, invoice_date) VALUES (:invoice_name, :invoice_date)');
        $invoice_flag = $invoice_statement->execute(array(
            'invoice_name' => trim($invoice['invoice_name']),
            'invoice_date' => trim($invoice['invoice_date']),
        ));

        $id = $this->getPDO()->lastInsertId();

//         insert single invoice
        $single_invoices = $array['invoice_subitem'];
        foreach ($single_invoices as $item) {
            $s_invoice_statement = $this->getPDO()->prepare('INSERT INTO invoice_subitem (number, order_number, address, suburb, invoice_number, delivery_date) VALUES (:number, :order_number, :address, :suburb, :invoice_number, :delivery_date)');
            $s_invoice_flag = $s_invoice_statement->execute(array(
                'number' => trim($item['number']),
                'order_number' => trim($item['order_number']),
                'address' => trim($item['address']),
                'suburb' => trim($item['suburb']),
                'invoice_number' => trim($item['invoice_number']),
                'delivery_date' => trim($item['delivery_date']),
            ));
        }

//        insert single invoice item
        $invoice_items = $array['product'];
        foreach ($invoice_items as $invoice_item) {
            foreach ($invoice_item as $item) {
                $item_statement = $this->getPDO()->prepare('INSERT INTO product (number, description) VALUES (:number, :description)');
                $item_flag = $item_statement->execute(array(
                    'number' => trim($item['number']),
                    'description' => trim($item['description']),
                ));
            }
        }

        $flag = ($invoice_flag && $s_invoice_flag && $item_flag) ? true : false;

        if ($flag) {
            return $id;
        } else {
            return false;
        }
    }
}