<?php
namespace Service;


use Model\Product;

class InvoiceProductLoader implements InvoiceLoaderInterface
{
    private $productStorage;
    private $number;

    public function __construct(PdoProductStorage $productStorage, $number)
    {
        $this->productStorage = $productStorage;
        $this->number = $number;
    }
    /**
     * @return Product[]
     */
    public function get()
    {
        $invoiceSubItems = array();

        $invoicesData = $this->productStorage->fetchAllInvoiceData($this->number);

        foreach($invoicesData as $invoiceData) {
            $invoiceSubItems[] = $this->createInvoiceFromData($invoiceData);
        }

        return $invoiceSubItems;
    }

    public function createInvoiceFromData(Array $invoiceData)
    {
        $product = new Product($invoiceData['number']);
        $product->setId($invoiceData['id']);
        $product->setDescription($invoiceData['description']);

//        return $product;
        return $product->jsonSerialize();
    }

}