<?php

require '../bootstrap.php';

use Service\Container;

$container = new Container($configuration);
$invoiceLoader = $container->getInvoiceLoader();
$invoices = $invoiceLoader->get();

echo json_encode(array(
    'error' => false,
    'invoices' => $invoices,
), JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);