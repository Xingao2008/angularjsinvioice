<?php

require '../bootstrap.php';

use Service\Container;

//$id = $_GET['id'];

$container = new Container($configuration);
//$invoiceLoader = $container->getInvoiceLoader();
//$invoice = $invoiceLoader->findOneById($id);
$invoiceSubItemsArray = array();

$invoiceSubItemLoader = $container->getInvoiceSubItemLoader();
$invoiceSubItems = $invoiceSubItemLoader->get();

foreach( $invoiceSubItems as $invoiceSubItem) {

    $productLoader = $container->getProductLoader($invoiceSubItem['number']);
    $products = $productLoader->get();
    $productsArray = array();

    foreach($products as $product) {
        $productsArray[] = array(
            'number' => $product['number'],
            'description' => $product['description']
        );
    }

    $invoiceSubItemsArray[] = array(
        'number' => $invoiceSubItem['number'],
        'order_number' => $invoiceSubItem['order_number'],
        'address' => $invoiceSubItem['address'],
        'suburb' => $invoiceSubItem['suburb'],
        'invoice_number' => $invoiceSubItem['invoice_number'],
        'delivery_date' => $invoiceSubItem['delivery_date'],
        'product' => $productsArray
    );
}

echo json_encode(array(
    'error' => false,
    'invoiceSubitems' => $invoiceSubItemsArray,
), JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);