<?php

require '../bootstrap.php';

use Service\Container;
$path = $_POST['invoice_upload'];

$container = new Container($configuration);
$path = '../resources/Invoice.dbf';
$invoiceLoader = $container->getInvoiceLoader();
$saveResult = $invoiceLoader->storeInvoice($path);
if ($saveResult !== false) {
    $invoice = $invoiceLoader->findOneById($saveResult);
} else {
    $invoice = false;
}
echo json_encode(array(
    'error' => false,
    'invoices' => $invoice,
), JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);