"use strict";

var app = angular.module('myApp', []);

    app.factory('helperFactory', function() {
        return {
            filterFileArrayByDone : function(thisArray, thisField, thisValue) {
                var arrayToReturn = [];
                for (var i = 0; i < thisArray.length; i++) {
                    if (thisArray[i].done == thisValue) {
                        arrayToReturn.push(thisArray[i][thisField]);
                    }
                }
                return arrayToReturn;
            }
        };
    });

    app.controller('InvoiceController', function ($scope, $http, $log, helperFactory) {
        var urlUpload = 'action/progress.php';
        var urlShow = 'action/select.php';
        var urlShowDetail = 'action/selectDetail.php';

        $scope.invoices = [];
        $scope.invoiceSubitems = [];
        $scope.file = '';

        function _invoiceUploadedSuccessfully(data) {
            return (
                data && !data.error && data.invoices
            );
        }

        $scope.clear = function() {
            $scope.file='';
        }

        $scope.upload = function() {
            $http({
                method : 'POST',
                url : urlUpload,
                data : "invoice_upload=" + $scope.invoice_upload,
                headers : {'Content-Type' : 'application/x-www-form-urlencoded'}
            })
                .success(function(data) {
                    if(_invoiceUploadedSuccessfully(data)) {
                        $scope.invoices.push({
                            id : data.invoices.id,
                            invoice_name : data.invoices.invoice_name,
                            invoice_date : data.invoices.invoice_date
                        });

                        $scope.clear();
                    }
                })
                .error(function(data, status, headers, config) {
                    throw new Error('Something went wrong with uploading');
                });
        };

        $scope.select = function() {
            $http.get(urlShow)
                .success(function(data) {
                    if(data.invoices) {
                        $scope.invoices = data.invoices;
                    }
                })
                .error(function(data, status, headers, config) {
                    throw new Error('Something went wrong with Selecting');
                });
        };

        $scope.selectDetail = function () {
            $http.get(urlShowDetail)
                .success(function(data) {
                    if(data.invoiceSubitems)
                    $scope.invoiceSubitems = data.invoiceSubitems;
                })
                .error(function() {
                    throw new Error('Something went wrong with Show Detail of Invoice');
                });
        }

        $scope.select();
    });

    app.directive('fileSelect', function() {
        var template = '<input type="file" name="files"/>';
        return function( scope, elem, attrs ) {
            var selector = $( template );
            elem.append(selector);
            selector.bind('change', function( event ) {
                scope.$apply(function() {
                    scope[ attrs.fileSelect ] = event.originalEvent.target.files;
                });
            });
            scope.$watch(attrs.fileSelect, function(file) {
                selector.val(file);
            });
        };
    });