<?php
/*
 * SETTINGS!
 */
$databaseName = 'tipple_invoice';
$databaseUser = 'root';
$databasePassword = '';

/*
 * CREATE THE DATABASE
 */
$pdoDatabase = new PDO('mysql:host=localhost', $databaseUser, $databasePassword);
$pdoDatabase->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$pdoDatabase->exec('CREATE DATABASE IF NOT EXISTS tipple_invoice');

/*
 * CREATE THE TABLE
 */
$pdo = new PDO('mysql:host=localhost;dbname='.$databaseName, $databaseUser, $databasePassword);
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

// initialize the table
$pdo->exec('DROP TABLE IF EXISTS invoice;');

$pdo->exec('CREATE TABLE `invoice` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `invoice_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
 `invoice_date` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci');


$pdo->exec('DROP TABLE IF EXISTS invoice_subitem;');

$pdo->exec('CREATE TABLE `invoice_subitem` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `nubmer` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
 `order_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
 `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
 `suburb` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
 `invoice_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
 `delivery_date` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci');


$pdo->exec('DROP TABLE IF EXISTS product;');

$pdo->exec('CREATE TABLE `product` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `nubmer` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
 `description` TEXT COLLATE utf8mb4_unicode_ci NOT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci');

/*
 * INSERT SOME DATA!
 */
echo "Ding!\n";
