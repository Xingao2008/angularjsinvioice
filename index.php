<html ng-app="myApp">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Tipple Invoice</title>

        <!-- Css -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="css/foundation.min.css" media="screen">
        <link rel="stylesheet" href="css/normalize.css" media="screen">
        <link rel="stylesheet" href="css/style.css" media="screen">
    </head>
    <body ng-controller="InvoiceController">
        <div class="row">
            <div class="column">
                <h1>Tipple Invoice</h1>
                    <div>
                        <form id="addFile" ng-submit="upload()">
                            <div class="row">
                                <div class="column">
                                    <span class="spanLabel warning">Please pick a invoice to upload</span>
                                </div>
                            </div>

                            <div class="row">
                                <div class="columns large-8">
                                    <div file-select="file"></div>
<!--                                    <input type="file" name="invoice_upload" ng-model="invoice_upload" ng-trim="false" placeholder="Please pick up invoice dbf file">-->
                                </div>
                            </div>

                            <div class="row">
                                <div class="column">
                                    <div class="flr">
                                        <button class="small button primary" type="submit"><i class="fa fa-cloud-upload"></i> Upload</button>
                                        <button class="small button alert" type="button" ng-click="clear()"><i class="fa fa-times"></i> Clear Entry</button>
                                    </div>
                                </div>
                            </div>
                        </form>

                        <form id="items">
                            <div class="row">
                                <div class="small-2 columns">
                                    <h5>Invoice Id</h5>
                                </div>
                                <div class="small-4 columns">
                                    <h5>Invoice Name</h5>
                                </div>
                                <div class="small-4 columns">
                                    <h5>Invoice Date</h5>
                                </div>
                            </div>

                            <div class="row" ng-repeat="invoice in invoices">
                                <div class="small-2 columns">
                                    <label for="invoice-{{ invoice.id }}">{{ invoice.id }}</label>
                                </div>

                                <div class="small-4 columns">
                                    <a href="#" data-reveal-id="myModal" ng-click="selectDetail()">{{ invoice.invoice_name }}</a>
                                </div>

                                <div class="small-4 columns">
                                    {{ invoice.invoice_date }}
                                </div>
                            </div>
                        </form>
                    </div>
            </div>
        </div>

        <!-- Popup Model -->
        <div id="myModal" class="reveal-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
            <form id="items">
                <div class="row">
                    <div class="small-1 columns">
                        <h5>Number</h5>
                    </div>
                    <div class="small-4 columns">
                        <h5>Order Number</h5>
                    </div>
                    <div class="small-1 columns">
                        <h5>Address</h5>
                    </div>
                    <div class="small-1 columns">
                        <h5>Suburb</h5>
                    </div>

                    <div class="small-3 columns">
                        <h5>Invoice Number</h5>
                    </div>

                    <div class="small-2 columns">
                        <h5>Delivery Date</h5>
                    </div>
                </div>

                <div class="row" ng-repeat="invoiceSubitem in invoiceSubitems">
                    <div class="small-1 columns">
                        <label for="invoiceSubitem-{{ invoiceSubitem.number }}">{{ invoiceSubitem.number }}</label>
                    </div>

                    <div class="small-4 columns">
                        {{ invoiceSubitem.order_number }}
                    </div>

                    <div class="small-1 columns">
                        {{ invoiceSubitem.address }}
                    </div>

                    <div class="small-1 columns">
                        {{ invoiceSubitem.suburb }}
                    </div>

                    <div class="small-3 columns">
                        {{ invoiceSubitem.invoice_number }}
                    </div>

                    <div class="small-2 columns">
                        {{ invoiceSubitem.delivery_date }}
                    </div>

                    <div class="large columns">
                        <div class="row" ng-repeat="product in invoiceSubitem.product">
                            <div class="columns">
                                {{ product.number }}
                            </div>

                            <div class="columns">
                                {{ product.description }}
                            </div>
                        </div>
                    </div>
                </div>

                <a class="close-reveal-modal">&#215;</a>
            </form>
        </div>

        <!-- JS -->
        <script src="js/angular.min.js"></script>
        <script src="js/app.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
        <script src="js/foundation.js"></script>
        <script src="js/foundation.reveal.js"></script>
        <script type="text/javascript">
            $(document).foundation();
        </script>

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </body>
</html>
